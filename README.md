# Taurus follow-up meetings

The objectives of the Taurus follow-up meetings are to:

* Share knowledge and experience
* Promote collaborative development
* Review user's feedback
* Classify and prioritize issues and pull requests

More information soon. Stay tuned!