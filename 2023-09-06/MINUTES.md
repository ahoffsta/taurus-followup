# Taurus Follow up meeting

Held on 6/09/2023 at 15:00.

Particpants:
- Solaris
  - Mateusz Floras
  - Michal Falowski
  - Wojciech Wantuch
  - Magdalena Szczepanik
- SOLEIL
  - Patrick Madela
- MAXIV
  - Mirjam Lindberg
- S2/MAXIV
  - Tomasz Madej
- ESRF
  - Reynald Bourtenbourg
  - Nicolas Leclercq
- ALBA
  - Zbigniew Reszela
  - Marti Caixal
  - Miquel Navarro
  - Jose Ramos
- ESO
    - Arturo Hoffstadt

## Minutes

1. Taurus Performance Optimization
- Zibi commented on the suggestion from Vincent on the last meeting about using ping() call before the subscription so we don't measure the initial connection time. This change was implemented in the tests but does not significantly change the results.
- Zibi asked if someone was able to take a look on the subscription benchmark results. Reynald commented he had tried to run them but had come into errors. Zibi and Reynald will try to figure out together on what is wrong.
- Miquel introduced the improvements regarding a bug in TangoAuthority. A MR is in progress. The improvement is significant on the startup time.
- Miquel described the default behavior where the creation of a Taurus Attribute subscribes to the ATTR_CONF_EVENT. Some applications may not need them, so avoiding the subscription would save some time. Using the Taurus custom settings "TANGO_AUTOSUBSCRIBE_CONF" disables the subscription. Still remains to be tested if it affects performance, though it should be a considerable improvement.
- Martí updated the reports of Case Studies. Now that there are new improvements, these measurements will be done again and see how they change. See the page 1 in https://gitlab.com/taurus-org/taurus-followup/-/blob/main/next_meeting/presentations/20230906_mcaixal.pdf
- Martí commented the status of the Taurus Core profiling. The Monkey Patch of Tango Device Proxy and Tango Attribute is completed. Now we are able to properly profile Taurus Core without "noise" from Tango. Once the TangoAuthority fix is merged, new profiling stats will be taken. Page 2 https://gitlab.com/taurus-org/taurus-followup/-/blob/main/next_meeting/presentations/20230906_mcaixal.pdf
- To analise these profiling results, on the last meeting, we talked about using Snakeviz. We found a problem where Snakeviz does not add up time properly in functions calls. We are not using Tuna, which does not have this problem. However, Tuna is not able to show the full call stack, where Snakeviz does show the full call stack and the amount of times each function is called.
- We asked whether someone has previous experience of profiling Taurus and other profiling tool suggestions. Arturo suggested https://github.com/bdarnell/plop. Mirjam suggested scalene. Page 3-5 https://gitlab.com/taurus-org/taurus-followup/-/blob/main/next_meeting/presentations/20230906_mcaixal.pdf

2. Taurus recent issues - round table.
- Zibi explained memory leaks in a taurus application used at ALBA BL29. This memory leak was due to the two issues in taurus that manifest when a Taurus Form is used with an attribute raising exceptions on readout. On every exception in Taurus we were connecting a slot the the same signal and inserting a spacer object. This issues was fixed by Emilio, and is currently being tested at one BL at ALBA and soon a MR will be opened to Taurus.
- Miquel explained an issue we recently experienced again at ALBA that was previously reported by other users: https://gitlab.com/taurus-org/taurus/-/issues/1160. The Taurus default formatter respects only the precission (number of digits after the coma) from the Tango format configuration, but doesn't respect other formats like the scientific notation format. This can be easily fixed by switching to the TangoFormatter. We think that in institutes using only Tango may want to use tauruscustomsetting.DEFAULT_FORMATTER = 'taurus.core.tango.util.tangoFormatter'.
- Marti mentioned an issue we found when trying to set a TaurusLabel background to tango state. This is currently not possible, only taurus state is possible (Ready, NotReady, Undefined).
- Arturo commented about an issue he found in his scheme plugin that could also affect Tango, it is related with classes: TangoAttrInfo and TangoAttr.  He will create an issue explaining it.
- Miriam commented on an issue relate wiht the Trend curve labels, we postponed this discussion to the point 3 of the agenda.
- Pattrick commented that they started using Taurus and hit a problem at the very beginnign with the TANGO_HOST using two databases - https://gitlab.com/taurus-org/taurus/-/merge_requests/938.  Reyanld confirmed that at the ESRF they found the same issues. Zibi confirmed that ALBA already planned to work on that.
- Wojciech commented on an issue with the Taurus Form and the motor widget in the Scan GUI - https://gitlab.com/taurus-org/taurus/-/issues/1301. He will work on providing a minimal example to reproduce this issue.

3. Taurus ongoing developments & merge request status.
- Zibi said there was no rogress on Taurus MR integration.
- Zibi commented that on the taurus_pyqtgraph we merged a statistics calculation for the trend thanks to Jakub from S2/MAXIV
- Jose and Tomasz introduced their approaches to renaming the curve names on plots, first a one setting for all curves and another one, specific per curve. We all agreed it would be nice to have both options. Probably better would be to access both from the same interface. Jose and Tomasz will work together on that. We also said it would be nice to have this settings persistent between the application startups.
- Zibi commented that  students at ALBA are working on svg synoptic and shared data manager: drags from the svg synoptic and drops on trend/forms and use SDM for bidireccional highlight between the paneles of the TaurusGUI and objects on the synoptic. Nicolas asked what is the status of the synoptics in Taurus. Miriam explained the svgsynoptics projects (orginal author Johan). Taurus also supports the old JDRAW approach to synoptics. Nicolas mentioned that for the ESRF synoptics are very important.

4. New releases.
- Zibi summarized that there will be two releases comming soon: taurus_pyqtgraph 0.7.0 with the statistics calculation and the taurus 5.1.7 with the performance improvements (TangoAuthority related). Later on there will be another taurus_pyqtgraph release with the possibility to change the curves labels.

5. Next steps in Taurus Community? - Brainstorming.
- Zibi noticed the big interest in the two first followup meetings and acknowledged the participants.
- Zibi explained that ALBA is currently focused on the Taurus Performance Optimization project.
- Zibi acknowledged numerous contributions received to Taurus after the March Taurus Workshop at the ESRF.
- Miriam asked how MAXIV could contribute to the project more since they rely havily on Taurus, especially their electronics engineers on the on trends. Zibi suggested to start opening issues and discussing together the best approach so solve problems and provide new features. Later we could make pair programming or peer reviews. 
- Nicolas announced that the ESRF and SOLEIL will participate in the Taurus training orgaznied by S2Innovation. Zibi asked if the training materials could be accessible to the community. Nicolas doubts that, but he will ask S2Innovation anyway.

6.  AOB
- Next follow-up meeting will take place on 18/10 (after ICALEPCS and before Taurus training for the ESRF and SOLEIL)
