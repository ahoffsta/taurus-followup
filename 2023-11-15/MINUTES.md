Held on 15/11/2023 at 15:00.

Participants:

- MAX IV
  - Benjamin Bertrand
  - Áureo Freitas
- S2 Innovation
  - Michał Gandor
- ESRF
  - Guillaume Communie
  - Nicolas Leclercq
- SOLARIS
  - Krzysztof Madura
  - Wojciech Wantuch
- SOLEIL
  - Ludmila Klenov
- ALBA
  - Martí Caixal
  - Emilio Morales
  - Miquel Navarro
  - Jose Ramos
- Other
  - Giradot
  
## Minutes

1. Taurus Performance Optimization.
    - M.Caixal explains the latests findings on two functions that can be improved. See [document](https://gitlab.com/taurus-org/taurus-followup/-/blob/main/2023-11-15/presentations/20231115_mcaixal.pdf?ref_type=heads) (Page 1 & 2)
    - N.Leclercq asked to Tango community about making optional the read of an attribute when making a subscription. It does not seem to be a difficult task and if it is done we could run tests to check for performance improvement.
    - N.Leclercq is interested to know more about the DelayedSubscriber function and how to use it. M.Navarro gives a brief explanation and E.Morales agrees to send extra information and possible code examples.

2. Taurus Recent issues - round table.
    - M.Caixal shows two new features will be implemented in Taurus PyQtGraph. See issue [120](https://gitlab.com/taurus-org/taurus_pyqtgraph/-/issues/120) and [121](https://gitlab.com/taurus-org/taurus_pyqtgraph/-/issues/121) .
   - M.Navarro gives a summary on the new issues and their status. See [document](https://gitlab.com/taurus-org/taurus-followup/-/blob/main/2023-11-15/presentations/TAURUS_follow_up_15112023.pdf?ref_type=heads) (Page 5).
    - L.Klenov asks how is the status of the [issue](https://gitlab.com/taurus-org/taurus/-/issues/1310). She comments she found a fix and if it can be implemented. M.Caixal explains that issues take time to be looked at and that if someone thinks they have a fix for the issue, they are more than welcome to create a MR, it will be faster to review implement this way.
    - M.Gandor would like to comment the [issue](https://gitlab.com/taurus-org/taurus/-/issues/1308) with Z.Reszela on how to approach a possible development of a migration script.
    - A. Freitas, ragarding the [issue](https://gitlab.com/taurus-org/taurus/-/issues/1315), asks if there is a known reason on why Taurus 5 does not allow to export trend data to .txt/.dat. They offer to create a MR with the feature they need, but before spending time on it they would like to know more about it. They will create an issue on GitLab to discuss it.

3. Ongroind developments & MR status.
    - M.Navarro comments on the status of [MR](https://gitlab.com/taurus-org/taurus/-/merge_requests/938). It has a lot of conclicts and will take longer than expected. Additionally, the same functions modified in the MR are the ones that will need to be modified as part of TPO. We do not have an ETA for it.
    - N.Leclercq comments on the status of [MR](https://gitlab.com/taurus-org/taurus_pyqtgraph/-/merge_requests/111). M.Navarro says it is being reviewed, but the MR changes the base class of two widgets, so it exposes a new API. It will need to be studied whether this can create a risk or not.

4. New releases.
   - J.Ramos shows a demo on the new Taurus Trend feature. See [MR](https://gitlab.com/taurus-org/taurus_pyqtgraph/-/merge_requests/107). Already merged and expected to be releases this week.
   - M.Caixal explains new feature for the latest release of Taurus. See [notes](https://gitlab.com/taurus-org/taurus/-/blob/develop/CHANGELOG.md#518-2023-11-09)
 
5. Others.
   - M.Navarro asked about the Taurus training in which ESRF and Soleil took part and N.Leclercq and gave feedback on it.
   - Next follow-up meeting will take place on 13/12/2023. 
