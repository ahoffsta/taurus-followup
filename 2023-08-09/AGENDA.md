# Taurus Follow up meeting

## Agenda

1. Highlights from ESRF Taurus Workshop. (5 min.)
2. Taurus Performance Optimization. (15 min.)
3. Taurus pending merge request status. (5 min.)
4. Taurus recent issues status. (5 min.)
5. New release. (5 min.)
6. Improvements in the docs - Tutorial for newcomers. (5 min)
7. Next steps in Taurus Community? - Brainstorming. (15 min.)
8. Others. (5 min.)
