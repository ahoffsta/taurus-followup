# Taurus Follow up meeting

Held on 9/08/2023 at 15:00.

Particpants:
- Solaris
    - Michal Piekarski
    - Michal Falowski
- MAXIV
    - Mirjam Lindberg
    - Vincent Hardion
- ESRF
    - Reynald Bourtenbourg
- ALBA
    - Zbigniew Reszela
    - Emilio Morales
    - Marti Caixal
    - Albert Ollé
- DESY
    - Teresa Nuñez
- ESO
    - Arturo Hoffstadt

## Minutes

1. Highlights from ESRF Taurus Workshop. (5 min.)

    - Zibi summarized the ESRF Taurus Workshop (Tango Community SIG) organized in March 2013
        where different institutes shared their Taurus applications and where we discussed
        Taurus Performance and Taurus Community issues.
    - This follow-up meeting is one of the outcomes of the workshop since there we said we wanted
        to organize such periodic project foollow-up meetigs.
    - On that Workshop we idenfified the following technical TODOs:
        - Take a closed look on issues explicitelly mentioned by Solaris:
            - Trend: adding new model deletes Y2 axis: https://gitlab.com/taurus-org/taurus_pyqtgraph/-/issues/104
            - Taurus GUI, trend: restoring configuration does not include curve config: https://gitlab.com/taurus-org/taurus_pyqtgraph/-/issues/112
            - TAURUS not react to Periodic Events: https://gitlab.com/taurus-org/taurus/-/issues/654
            - These are still on our TODO list.
        - Type hinting e.g. typing, traits, stubs, etc. suggested by ESRF to facilitate developers life: IDE hints, type checking.
            - We could reuse Johan's contribution to Sardana (based on reusing type hints from docstring and tests) since Taurus and Sardana were developed using the same programming practices.
        - QuickGUI (define GUIs in YAML) developed at MAX IV
            - This is very similar to [Config loaders MR](https://gitlab.com/taurus-org/taurus/-/merge_requests/1116) proposed by Stanislaw from SOLARIS. We should probably evalute these contributions together.
        - Polish taurus_pyqtgraph
            - taurus_pyqtraph is activelly developed and several improvements are in progress.
        - Silx for 2D plotting
            - This is still on our TODO list.
        - Consult with ATK, QTango performance problems
            - During the Tango Meeting we were in touch with Giacomo and we know that Cumbia does subscription in multiples threads, and shows the GUI almost immediatelly and updates the widgets state later. Cumbia does not subscribe to configuration change events.

2. Taurus Performance Optimization. (15 min.)

    - Marti introduced the TPO project and shared the first results - see materials from the presentations folder.
    - Marti hihglighted an issue with multiple creation of Database objects. Vincent asked why Taurus uses an explicit Database object while it is not necessary to read, write and subscribe to attribute events. Zibi explained three model types in Taurus: Authority, Device and Attribute, their parental relationships, their Singleton nature and the role of the Factory. The issue must be a simple to fix bug.
    - For measurement of PyTango performance while subsribing to events, Vincent suggested to use an early `ping()` in the measurements to avoid first connection affecting the measurement.
    - Reynald suggested to try the new Tango Database where Elettra improved the Database behavior by using InnoDB instead of MyISAM scheme.
    - Reyanld will take a deeper look on the PyTango performance subscription results.
    
3. Taurus pending merge request status. (5 min.)
    - Emilio reviewed the pending MRs - see materials from the presentations folder.
    - Emilio highlighted that help in reviewing MR would be very welcome.
    
4. Taurus recent issues status. (5 min.)
    - Emilio reviewed issues created since March 2023 (Taurus Workshop at the ESRF) - see materials from the presentations folder.
    - Emilio highlighted that help in repling to issues would be very welcome.

5. New release. (5 min.)
    - Emilio reviewed unofficial releases in Taurus and announced that soon we will make a taurus_pyqtgraph release since the last release is broken with the newest pyqtgraph - see materials from the presentations folder.
    - Zibi explained that unofficial release do not pass extensive manual tests as in the past were passing two biannual official releases.
    - Zibi asked S2/MAXIV colleagus for some news about the [statistics tool MR](https://gitlab.com/taurus-org/taurus_pyqtgraph/-/merge_requests/104) from Jakub. Michal G. contacted him and Jakub soom will let us know if we should wait for this MR for the taurus_pyqtgraph release.
    
6. Improvements in the docs - Tutorial for newcomers. (5 min)
    - Zibi reminded that the ESRF announced on the last Tango meeting that they will start using Taurus in the Control Room applications. As it was identified the ESRF will need some training on different levels: TaurusGUI, Taurus designer, widget development and core developement. Reynald confirmed that but there are no other news about the training. Zibi thinks it would be nice to make the eventual materials from such a trainining available for the Taurus Community.

7. Next steps in Taurus Community? - Brainstorming. (15 min.)
    - Emilio sumarized the ideas for revival of the Taurus Community we identified during the Taurus Workshop at the ESRF - see materials from the presentations folder.
    - Miriam commented that this kind of follow-up meeting are good for the project. From MAXIV they will try to identify a person that could come regularly to the meetings. Zibi said that we could even assign the GitLab project Maintainer role to these persons.
    - We agreed to try to organize one follow-up meeting every four weeks and try to keep it on Wednesday at 15:00. Next meeting will be held on 6th of September.
    
11. Others. (5 min.)
    - Arturo introduced the issue he created: [Add support for PyQt6/PySide6 ](https://gitlab.com/taurus-org/taurus/-/issues/1299) and asked for feedback. He asked if it was needed to define a TEP. Zibi said that we need to decide together (as Taurus Community) if we want to continue using TEP workflow. We would need to evaluate if this issue is "big" enough for a TEP. Zibi said that in the TPO project we also doubt if we should create a TEP or at least up to now we did not have time to create one.
