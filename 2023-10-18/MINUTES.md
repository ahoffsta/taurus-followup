Held on 18/10/2023 at 15:00.

Particpants:
- SOLEIL
  - Patrick Madela
- MAXIV
  - Antonio Bartalesi
  - Benjamin Bertrand
  - Vincent Hardion
- ESRF
  - Guillaume Communie
  - Nicolas Leclercq
- ALBA
  - Marti Caixal
  - Emilio Morales
  - Miquel Navarro
  - Jose Ramos
- ESO
    - Arturo Hoffstadt

## Minutes

1. Taurus Performance Optimization
    - M.Caixal explains latest imprevements with healthy attributes. See https://gitlab.com/taurus-org/taurus-followup/-/blob/main/2023-10-18/presentations/20231018_mcaixal.pdf (Page 1.)
    - N.Leclerc took note of one requests to the TANGO community to improve TAURUS performance.
    - M.Navarro, M.Caixal and E.Morales will talk with Z.Reszela about TANGO community requests and will inform N.Leclerc.

2. Taurus recent issues - round table.
    - M.Navarro talked about issue he found and report at https://gitlab.com/taurus-org/taurus/-/issues/1304
    - E.Morales talked about Memory Leak found. See https://gitlab.com/taurus-org/taurus-followup/-/blob/main/2023-10-18/presentations/TAURUS_folowup_meeting_18102023.pdf (Slides 5 to 11).
    - B.Bertrand comments that they are experiencing problems with dots and commas. He highlihgted that it would be interesting to solve issue https://gitlab.com/taurus-org/taurus/-/issues/677.
    - A.Hoffstadt comments that he has found a workaround to issue https://gitlab.com/taurus-org/taurus/-/issues/1283 

3. Taurus ongoing developments & merge request status.
    - M.Navarro and E.Morales talked about issue https://gitlab.com/taurus-org/taurus/-/issues/1233
    - M.Caixal talked about issue https://gitlab.com/taurus-org/taurus/-/merge_requests/938 and he is trying to merge it but he found some conflicts. He is working on it.

4. New releases.
    - M.Caixal talked about new release of taurus-pyqtgraph 0.7.0 with new statistics widget.
    - M.Navarro talked about new TAURUS unnoficial release 5.1.7 that includes latest optimizations. He encouraged to install and test it.

5. Others.
    - N.Leclerc comments about TAURUS trainig for ESRF and Soleil. S2Innovation trainig will be held form 25 to 27 of October.
    - N.Leclerc comments that ESRF currently have 2 TAURUS applications running in production.
    - N.Leclerc comment that Guillaume Communie will represent ESRF on TAURUS follow-up meetings.
    - E.Morales proposed next meeting will be held on 15.11.2023.